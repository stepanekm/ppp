/**
 * @file    parallel_heat_solver.h
 * @author  xlogin00 <xlogin00@stud.fit.vutbr.cz>
 *
 * @brief   Course: PPP 2019/2020 - Project 1
 *
 * @date    2020-MM-DD
 */

#ifndef PARALLEL_HEAT_SOLVER_H
#define PARALLEL_HEAT_SOLVER_H

#include "base_heat_solver.h"

/**
 * @brief The ParallelHeatSolver class implements parallel MPI based heat
 *        equation solver in 2D using 1D and 2D block grid decomposition.
 */
class ParallelHeatSolver : public BaseHeatSolver
{
    //============================================================================//
    //                            *** BEGIN: NOTE ***
    //
    // Modify this class declaration as needed.
    // This class needs to provide at least:
    // - Constructor which passes SimulationProperties and MaterialProperties
    //   to the base class. (see below)
    // - Implementation of RunSolver method. (see below)
    //
    // It is strongly encouraged to define methods and member variables to improve
    // readability of your code!
    //
    //                             *** END: NOTE ***
    //============================================================================//

public:
    /**
     * @brief Constructor - Initializes the solver. This should include things like:
     *        - Construct 1D or 2D grid of tiles.
     *        - Create MPI datatypes used in the simulation.
     *        - Open SEQUENTIAL or PARALLEL HDF5 file.
     *        - Allocate data for local tile.
     *        - Initialize persistent communications?
     *
     * @param simulationProps Parameters of simulation - passed into base class.
     * @param materialProps   Parameters of material - passed into base class.
     */
    ParallelHeatSolver(SimulationProperties &simulationProps, MaterialProperties &materialProps);
    virtual ~ParallelHeatSolver() {};

    /**
     * @brief Run main simulation loop.
     * @param outResult Output array which is to be filled with computed temperature values.
     *                  The vector is pre-allocated and its size is given by dimensions
     *                  of the input file (edgeSize*edgeSize).
     *                  NOTE: The vector is allocated (and should be used) *ONLY*
     *                        by master process (rank 0 in MPI_COMM_WORLD)!
     */
    virtual void RunSolver(std::vector<float, AlignedAllocator<float> > &outResult);

protected:
    void createMPIDatatypes();
    void createMPICommunicators();
    float ComputeMiddleColAvgTemp(const float* data) const;
    void SendEdges(float* data);
    void RecvPaddings(float* data);
    void ExchangeHaloZones(float* data);
    void ParallelStoreDataIntoFile(hid_t fileHandle, size_t iteration, const float *data);

    int m_rank;     ///< Process rank in global (MPI_COMM_WORLD) communicator.
    int m_size;     ///< Total number of processes in MPI_COMM_WORLD.
    AutoHandle<hid_t> m_fileHandle;
    std::vector<float, AlignedAllocator<float> > m_resArray;
    std::vector<float, AlignedAllocator<float> > m_tmpArray;
    std::vector<float, AlignedAllocator<float> > m_domainParams;
    std::vector<int, AlignedAllocator<int> > m_domainMap;
    std::vector<int> m_counts, m_displ;
    MPI_Comm middleColComm;
    MPI_Datatype tileFloatT, gridFloatT;
    MPI_Datatype tileIntT, gridIntT;
    MPI_Datatype paddingTs[4];
    MPI_Datatype edgeTs[4];
    MPI_Request req[4];
    int m_neighbors[4];
    int m_middleColIndex, m_leadrerOfMiddleCol;
    int m_edgeSize, m_numTilesX, m_numTilesY;
    int m_tileSizeX, m_tileSizeY;
    int m_myTileStartX, m_myTileStartY;
    int m_myTileSizeX, m_myTileSizeY;

};

#endif // PARALLEL_HEAT_SOLVER_H
