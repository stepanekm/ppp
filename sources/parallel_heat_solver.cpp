/**
 * @file    parallel_heat_solver.cpp
 * @author  xlogin00 <xlogin00@stud.fit.vutbr.cz>
 *
 * @brief   Course: PPP 2019/2020 - Project 1
 *
 * @date    2020-MM-DD
 */

#include "parallel_heat_solver.h"
#include <mpi.h>

#define UP 0
#define RIGHT 1
#define DOWN 2
#define LEFT 3

ParallelHeatSolver::ParallelHeatSolver(SimulationProperties &simulationProps,
                                       MaterialProperties &materialProps)
    : BaseHeatSolver (simulationProps, materialProps),
      m_fileHandle(H5I_INVALID_HID, static_cast<void (*)(hid_t )>(nullptr))
{
    MPI_Comm_size(MPI_COMM_WORLD, &m_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &m_rank);

    if(!m_simulationProperties.GetOutputFileName().empty())
    {
        if(m_simulationProperties.IsUseParallelIO())
        {
            hid_t plist_id = H5Pcreate(H5P_FILE_ACCESS);
            H5Pset_fapl_mpio(plist_id, MPI_COMM_WORLD, MPI_INFO_NULL);
            m_fileHandle.Set(H5Fcreate(simulationProps.GetOutputFileName("par").c_str(),
                    H5F_ACC_TRUNC, H5P_DEFAULT, plist_id), H5Fclose);
        }
        else if(m_rank == 0)
        {
            m_fileHandle.Set(H5Fcreate(simulationProps.GetOutputFileName("par").c_str(),
                    H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT), H5Fclose);
        }
    }

    m_simulationProperties.GetDecompGrid(m_numTilesX, m_numTilesY);
    m_edgeSize = m_materialProperties.GetEdgeSize();
    m_tileSizeX = m_edgeSize / m_numTilesX;
    m_tileSizeY = m_edgeSize / m_numTilesY;

    createMPIDatatypes();
    createMPICommunicators();

    m_neighbors[UP] = (m_rank >= m_numTilesX) ? m_rank - m_numTilesX : -1;
    m_neighbors[RIGHT] = ((m_rank + 1) % m_numTilesX != 0) ? m_rank + 1 : -1;
    m_neighbors[DOWN] = (m_size - m_rank > m_numTilesX) ? m_rank + m_numTilesX : -1;
    m_neighbors[LEFT] = (m_rank % m_numTilesX != 0) ? m_rank - 1 : -1;

    m_myTileStartX = (m_neighbors[LEFT] != -1) ? 2 : 4;
    m_myTileStartY = (m_neighbors[UP] != -1) ? 2 : 4;

    m_myTileSizeX = m_tileSizeX;
    m_myTileSizeY = m_tileSizeY;
    if(m_neighbors[UP] == -1)
        m_myTileSizeY -= 2;
    if(m_neighbors[LEFT] == -1)
        m_myTileSizeX -= 2;
    if(m_neighbors[DOWN] == -1)
        m_myTileSizeY -= 2;
    if(m_neighbors[RIGHT] == -1)
        m_myTileSizeX -= 2;

    int tileElemsWithPadding = (m_tileSizeX + 4) * (m_tileSizeY + 4);
    m_resArray.resize(tileElemsWithPadding);
    m_domainParams.resize(tileElemsWithPadding);
    m_domainMap.resize(tileElemsWithPadding);

    m_counts.reserve(m_size);
    for(int i = 0; i < m_size; ++i)
        m_counts.push_back(1);

    m_displ.reserve(m_size);
    for(int i = 0; i < m_numTilesY; ++i)
        for(int j = 0; j < m_numTilesX; ++j)
            m_displ.push_back(m_edgeSize * i * m_tileSizeY + j * m_tileSizeX);

    MPI_Scatterv(m_materialProperties.GetInitTemp().data(), m_counts.data(), m_displ.data(), gridFloatT, m_resArray.data(), 1, tileFloatT, 0, MPI_COMM_WORLD);
    MPI_Scatterv(m_materialProperties.GetDomainParams().data(), m_counts.data(), m_displ.data(), gridFloatT, m_domainParams.data(), 1, tileFloatT, 0, MPI_COMM_WORLD);
    MPI_Scatterv(m_materialProperties.GetDomainMap().data(), m_counts.data(), m_displ.data(), gridIntT, m_domainMap.data(), 1, tileIntT, 0, MPI_COMM_WORLD);
}

void ParallelHeatSolver::createMPIDatatypes()
{
    MPI_Datatype gridFloatTmpT, gridIntTmpT;
    int sizes[2] = {m_tileSizeY + 4, m_tileSizeX + 4};
    int subsizes[2] = {m_tileSizeY, m_tileSizeX};
    int starts[2] = {2, 2};
    MPI_Type_create_subarray(2, sizes, subsizes, starts, MPI_ORDER_C, MPI_FLOAT, &tileFloatT);
    MPI_Type_create_subarray(2, sizes, subsizes, starts, MPI_ORDER_C, MPI_INT, &tileIntT);
    MPI_Type_commit(&tileFloatT);
    MPI_Type_commit(&tileIntT);

    sizes[0] = m_edgeSize;
    sizes[1] = m_edgeSize;
    subsizes[0] = m_tileSizeY;
    subsizes[1] = m_tileSizeX;
    starts[0] = 0;
    starts[1] = 0;
    MPI_Type_create_subarray(2, sizes, subsizes, starts, MPI_ORDER_C, MPI_FLOAT, &gridFloatTmpT);
    MPI_Type_create_subarray(2, sizes, subsizes, starts, MPI_ORDER_C, MPI_INT, &gridIntTmpT);
    MPI_Type_commit(&gridFloatTmpT);
    MPI_Type_commit(&gridIntTmpT);

    MPI_Type_create_resized(gridFloatTmpT, 0, 1 * sizeof(float), &gridFloatT);
    MPI_Type_create_resized(gridIntTmpT, 0, 1 * sizeof(float), &gridIntT);
    MPI_Type_commit(&gridFloatT);
    MPI_Type_commit(&gridIntT);

    sizes[0] = m_tileSizeY + 4;
    sizes[1] = m_tileSizeX + 4;
    int paddingEdgeSubsizes[4][2] = {{2, m_tileSizeX}, {m_tileSizeY, 2}, {2, m_tileSizeX}, {m_tileSizeY, 2}};
    int paddingStarts[4][2] = {{0, 2}, {2, m_tileSizeX + 2}, {m_tileSizeY + 2, 2}, {2, 0}};
    int edgeStarts[4][2] = {{2, 2}, {2, m_tileSizeX}, {m_tileSizeY, 2}, {2, 2}};
    for(int i = 0; i < 4; ++i)
    {
        MPI_Type_create_subarray(2, sizes, paddingEdgeSubsizes[i], paddingStarts[i], MPI_ORDER_C, MPI_FLOAT, paddingTs + i);
        MPI_Type_create_subarray(2, sizes, paddingEdgeSubsizes[i], edgeStarts[i], MPI_ORDER_C, MPI_FLOAT, edgeTs + i);
        MPI_Type_commit(paddingTs + i);
        MPI_Type_commit(edgeTs + i);
    }
}

void ParallelHeatSolver::createMPICommunicators()
{
    if(m_rank % m_numTilesX == m_numTilesX / 2)
    {
        MPI_Comm_split(MPI_COMM_WORLD, 0, m_rank / m_numTilesX, &middleColComm);
        m_middleColIndex = (m_numTilesX == 1) ? m_edgeSize / 2 + 2 : 2;
    }
    else
    {
        MPI_Comm_split(MPI_COMM_WORLD, MPI_UNDEFINED, m_rank, &middleColComm);
        m_middleColIndex = -1;
    }

    m_leadrerOfMiddleCol = m_numTilesX / 2;
}

void ParallelHeatSolver::SendEdges(float* data)
{
    int neighbor;
    for(int i = 0; i < 4; ++i)
    {
        neighbor = m_neighbors[i];
        if(neighbor != -1)
        {
            MPI_Isend(data, 1, edgeTs[i], neighbor, 42, MPI_COMM_WORLD, req + i);
        }
    }
}

void ParallelHeatSolver::RecvPaddings(float* data)
{
    int neighbor;
    for(int i = 0; i < 4; ++i)
    {
        neighbor = m_neighbors[i];
        if(neighbor != -1)
        {
            MPI_Recv(data, 1, paddingTs[i], neighbor, 42, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
            MPI_Wait(req + i, MPI_STATUS_IGNORE);
        }
    }
}

void ParallelHeatSolver::ExchangeHaloZones(float* data)
{
    SendEdges(data);
    RecvPaddings(data);
}

void ParallelHeatSolver::ParallelStoreDataIntoFile(hid_t fileHandle, size_t iteration, const float *data)
{
    hsize_t gridSize[] = { m_edgeSize, m_edgeSize };

    // Create new HDF5 file group named as "Timestep_N", where "N" is number
    //    of current snapshot. The group is placed into root of the file "/Timestep_N".
    std::string groupName = "Timestep_" + std::to_string(static_cast<unsigned long long>(iteration / m_simulationProperties.GetDiskWriteIntensity()));
    AutoHandle<hid_t> groupHandle(H5Gcreate(fileHandle, groupName.c_str(),
                                            H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT), H5Gclose);
    {
        // Create new dataset "/Timestep_N/Temperature" which is simulation-domain
        //  sized 2D array of "float"s.
        std::string dataSetName("Temperature");
        // Define shape of the dataset (2D edgeSize x edgeSize array).
        AutoHandle<hid_t> filespace(H5Screate_simple(2, gridSize, NULL), H5Sclose);
        hsize_t tileWithPaddingSize[] = {m_tileSizeY + 4, m_tileSizeX + 4};
        AutoHandle<hid_t> memspace(H5Screate_simple(2, tileWithPaddingSize, NULL), H5Sclose);

        // Create hyperslab for filespace
        hsize_t filespaceStart[2] = {(m_rank / m_numTilesX) * m_tileSizeY, (m_rank % m_numTilesX) * m_tileSizeY};
        hsize_t filespaceCount[2] = {m_tileSizeY, m_tileSizeX};
        H5Sselect_hyperslab(filespace, H5S_SELECT_SET, filespaceStart, NULL, filespaceCount, NULL);

        // Create hyperslab for memspace
        hsize_t memspaceStart[2] = {2, 2};
        hsize_t memspaceCount[2] = {m_tileSizeY, m_tileSizeX};
        H5Sselect_hyperslab(memspace, H5S_SELECT_SET, memspaceStart, NULL, memspaceCount, NULL);

        // Enable chunking
        hsize_t chunk[2] = {m_tileSizeY, m_tileSizeX};
        AutoHandle<hid_t> chunkPropertyList(H5Pcreate(H5P_DATASET_CREATE), H5Pclose);
        H5Pset_chunk(chunkPropertyList, 2, chunk);

        // Create dataset
        AutoHandle<hid_t> dataSetHandle(H5Dcreate(groupHandle, dataSetName.c_str(),
                                                  H5T_NATIVE_FLOAT, filespace,
                                                  H5P_DEFAULT, chunkPropertyList, H5P_DEFAULT), H5Dclose);

        // Set collective mode
        AutoHandle<hid_t> dxplPropertyList(H5Pcreate(H5P_DATASET_XFER), H5Pclose);
        H5Pset_dxpl_mpio(dxplPropertyList, H5FD_MPIO_COLLECTIVE);

        // Write data
        H5Dwrite(dataSetHandle, H5T_NATIVE_FLOAT, memspace, filespace, dxplPropertyList, data);
    }

    {
        // 3. Create Integer attribute in the same group "/Timestep_N/Time"
        //    in which we store number of current simulation iteration.
        std::string attributeName("Time");

        // 3.1 Dataspace is single value/scalar.
        AutoHandle<hid_t> dataSpaceHandle(H5Screate(H5S_SCALAR), H5Sclose);

        // 3.2 Create the attribute in the group as double.
        AutoHandle<hid_t> attributeHandle(H5Acreate2(groupHandle, attributeName.c_str(),
                                                     H5T_IEEE_F64LE, dataSpaceHandle,
                                                     H5P_DEFAULT, H5P_DEFAULT), H5Aclose);

        // 3.3 Write value into the attribute.
        if(m_rank == 0)
        {
            double snapshotTime = double(iteration);
            H5Awrite(attributeHandle, H5T_IEEE_F64LE, &snapshotTime);
        }

        // NOTE: Both dataspace and attribute handles will be released here.
    }
}


void ParallelHeatSolver::RunSolver(std::vector<float, AlignedAllocator<float> > &outResult)
{
    m_tmpArray = m_resArray;
    float *workArrays[] = { m_resArray.data(), m_tmpArray.data() };
    float middleColAvgTemp = 0;

    ExchangeHaloZones(m_domainParams.data());
    ExchangeHaloZones(workArrays[0]);

    double startTime = MPI_Wtime();

    for(size_t iter = 0; iter < m_simulationProperties.GetNumIterations(); ++iter)
    {
        // Compute edges first
        for(int i = m_myTileStartY; i < m_myTileStartY + m_myTileSizeY; ++i)
        {
            for(int j = m_myTileStartX; j < m_myTileStartX + m_myTileSizeX; ++j)
            {
                ComputePoint(workArrays[0], workArrays[1],
                    m_domainParams.data(), m_domainMap.data(),
                    i, j,
                    m_tileSizeX + 4,
                    m_simulationProperties.GetAirFlowRate(),
                    m_materialProperties.GetCoolerTemp());

                // For rows in the middle, skip the middle columns
                if(j == m_myTileStartX + 1 && i > m_myTileStartY + 1 && i < m_myTileStartY + m_myTileSizeY - 2)
                    j += m_myTileSizeX - 4;
            }
        }

        SendEdges(workArrays[1]);

        // Compute rest of the tile
        UpdateTile(workArrays[0], workArrays[1],
            m_domainParams.data(), m_domainMap.data(),
            m_myTileStartX + 2, m_myTileStartY + 2,
            m_myTileSizeX - 4, m_myTileSizeY - 4,
            m_tileSizeX + 4,
            m_simulationProperties.GetAirFlowRate(),
            m_materialProperties.GetCoolerTemp());

        RecvPaddings(workArrays[1]);

        std::swap(workArrays[0], workArrays[1]);

        if(!m_simulationProperties.GetOutputFileName().empty() &&
            iter % m_simulationProperties.GetDiskWriteIntensity() == 0)
        {
            if(m_simulationProperties.IsUseParallelIO()){
                ParallelStoreDataIntoFile(m_fileHandle, iter, workArrays[0]);
            }
            else
            {
                MPI_Gatherv(workArrays[0], 1, tileFloatT, outResult.data(), m_counts.data(), m_displ.data(), gridFloatT, 0, MPI_COMM_WORLD);
                if(m_rank == 0)
                    StoreDataIntoFile(m_fileHandle, iter, outResult.data());
            }
        }

        if(ShouldPrintProgress(iter))
        {
            middleColAvgTemp = ComputeMiddleColAvgTemp(workArrays[0]);
            if(m_rank == m_leadrerOfMiddleCol)
                PrintProgressReport(iter, middleColAvgTemp);
        }
    }
    MPI_Gatherv(workArrays[0], 1, tileFloatT, outResult.data(), m_counts.data(), m_displ.data(), gridFloatT, 0, MPI_COMM_WORLD);

    double elapsedTime = MPI_Wtime() - startTime;
    middleColAvgTemp = ComputeMiddleColAvgTemp(workArrays[0]);
    if(m_rank == m_leadrerOfMiddleCol)
        PrintFinalReport(elapsedTime, middleColAvgTemp, "par");
}

float ParallelHeatSolver::ComputeMiddleColAvgTemp(const float *data) const
{
    // This process does not hold middle of the grid
    if(m_middleColIndex == -1)
        return -1;

    float middleColAvgTemp = 0.0f, sumOfMiddleCol;
    for(size_t i = 2; i < m_tileSizeY + 2; ++i)
        middleColAvgTemp += data[i * (m_tileSizeX + 4) + m_middleColIndex];

    // Count sum of middle column into the leader of that column
    MPI_Reduce(&middleColAvgTemp, &sumOfMiddleCol, 1, MPI_FLOAT, MPI_SUM, 0, middleColComm);

    // This functions gives valid result only for the leader of the middle column
    return sumOfMiddleCol / m_edgeSize;
}
