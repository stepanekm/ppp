import matplotlib.pyplot as plt
import csv

x256  = []
x512  = []
x1024  = []
x2048  = []
x4096 = []

y256  = []
y512  = []
y1024  = []
y2048  = []
y4096 = []

mpi_procs = 0
omp_threads = 3
domain_size = 4
iteration_time = -1

nowrite = {}
write = {}
pwrite = {}


def tr(arr, i):
    return [arr[index] for index in range(i, len(arr), 3)]

# mpi_procs;grid_tiles_x;grid_tiles_y;omp_threads;domain_size;n_iterations;disk_write_intensity;airflow;material_file;output_file;simulation_mode;middle_col_avg_temp;total_time;iteration_time
with open('run_full_mpi_2d_out.csv','r') as csvfile:
    plots = csv.reader(csvfile, delimiter=';')
    for ord, row in enumerate(plots):
        if ord == 0:
            continue
        index = int(row[domain_size]) / ( int(row[mpi_procs]) ** 0.5)
        if ord%3 == 0:
            if not index in nowrite:
                nowrite[index] = []
            nowrite[index].append((int(row[mpi_procs]), float(row[iteration_time])))
        if ord%3 == 1:
            if not index in write:
                write[index] = []
            write[index].append((int(row[mpi_procs]),float( row[iteration_time])))
        if ord%3 == 2:
            if not index in pwrite:
                pwrite[index] = []
            pwrite[index].append((int(row[mpi_procs]), float(row[iteration_time])))







plt.xscale("log", basex=2)
plt.yscale("log", basey=2)
size = 4

xs = [1024, 512, 256, 128]

nowrite = { k:v  for (k,v) in nowrite.items() if len(v) > 1 and k in xs}
write = { k:v  for (k,v) in write.items() if len(v) > 1 and k in xs}
pwrite = { k:v  for (k,v) in pwrite.items() if len(v) > 1 and k in xs}
fig, ax = plt.subplots(nrows=1, ncols=3, sharey=True, figsize=(10, 5))
for i in range(3):
    if i == 0:
        ggg = sorted(list(nowrite.items()), key=lambda x:x[0])

    if i == 1:
        ggg = sorted(list(write.items()), key=lambda x:x[0])
    if i == 2:
        ggg = sorted(list(pwrite.items()), key=lambda x:x[0])
    for k, v in ggg:
        ax[i].plot([x[0] for x in v], [x[1] for x in v], label=int(k), marker='o')

    if i % 3 == 0:
        mode = 'No disk write'
    elif i % 3 == 1:
        mode = 'One proces writes'
    else:
        mode = 'Parallel write'

    ax[i].set(xlabel='Processes', ylabel='Time for iteration [s]', title='Weak scaling: {0}'.format(mode))
    ax[i].set_xscale('log', basex=2)
    ax[i].set_yscale('log', basey=2)
    ax[i].grid()

handles, labels = ax[0].get_legend_handles_labels()
fig.legend(handles, labels, title='Input\nsize per\nprocessor')

fig.savefig('weak.png')
#plt.legend()
#fig.show()
